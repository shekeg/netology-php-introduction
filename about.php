<?php
$name = 'Евгений';
$age = 25;
$photo = 'user.jpg';
$email = 'shek.e.g@yandex.ru';
$city = 'Байконур';
$about = 'Ведущий специалист отдела информатизации';
?>
<!DOCTYPE>
<html lang="ru">
    <head>
        <title>Страцница пользователя<?= $name ?></title>
        <meta charset="utf-8">
        <link href="https://fonts.googleapis.com/css?family=Roboto&amp;subset=cyrillic" rel="stylesheet">
        <link rel="stylesheet" href="main.css">
    </head>
    <body>
      <div class="warpper">
        <div class="user-card">
          <div class="user-header">
            <img class="user-photo" src="<?= $photo ?>" alt="user photo">
            <dl class="user-header__text">
              <dt>Имя</dt>
              <dd><?= $name ?></dd>
              <dt>О себе</dt>
              <dd><?= $about ?></dd>
            </dl>
          </div>
          <div class="user-content">
            <dl class="user-content__text">
              <dt>Возраст</dt>
              <dd><?= $age ?></dd>
              <dt>Адрес электронной почты</dt>
              <dd><a href="mailto:<?= $email ?>"><?= $email ?></a></dd>
              <dt>Город</dt>
              <dd><?= $city ?></dd>
            </dl>
          </div>
        </div>
      </div>
    </body>
</html>